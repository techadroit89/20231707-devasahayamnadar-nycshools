package com.app.nycschooldemo.presentation.school_list.viewmodel

import com.app.nycschooldemo.core.exceptions.NoDataError
import com.app.nycschooldemo.data.repository.SharedRepository
import com.app.nycschooldemo.domain.GetSchoolListUseCase
import com.app.nycschooldemo.rule.ViewModelTestRule
import com.appmattus.kotlinfixture.kotlinFixture
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolListViewModelTest {

    val dispatcher = UnconfinedTestDispatcher()
    @get:Rule
    val rule = ViewModelTestRule()
    val fixture = kotlinFixture()

    lateinit var viewModel: SchoolListViewModel

    @MockK
    lateinit var useCase: GetSchoolListUseCase
    @MockK
    lateinit var sharedRepository: SharedRepository
    val initialState = SchoolListState()
    val schoolInfoList = fixture<List<SchoolInfo>>()

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        coEvery { useCase.invoke() } returns flowOf(schoolInfoList)
        viewModel = SchoolListViewModel(
            useCase = useCase,
            state = initialState,
            dispatcher = dispatcher,
            repository = sharedRepository
        )
    }

    @Test
    fun `check the emitted state on load school list successfully`() = runTest(
        dispatcher
    ) {

        // The viewModel.loadSchoolList() action is called in constructor invocation and the states are emitted.
        // So we reset the initial state.
        viewModel.setState { initialState }

        // Represent the list of expected states including initial states
        val expectedStates = listOf(
            initialState,
            initialState.showLoading(),
            initialState.onLoad(schoolInfoList)
        )

        // Capture all the emitted states
        val emittedStates = mutableListOf<SchoolListState>()

        // Use background scope as this scope is closed after the test is completed
        backgroundScope.launch { viewModel.stateFlow.toList(emittedStates) }

        viewModel.loadSchoolList()

        assertEquals(expectedStates, emittedStates)
    }

    @Test
    fun `check the error in emitted state on load school list action`() = runTest(
        dispatcher
    ) {

        // Throw no Data Error
        coEvery { useCase.invoke() } returns flow { throw NoDataError }

        // The viewModel.loadSchoolList() action is called in constructor invocation and the states are emitted.
        // So we reset the initial state.
        viewModel.setState { initialState }

        // Represent the list of expected states including initial states
        val expectedStates = listOf(
            initialState,
            initialState.showLoading(),
            initialState.onErrorResult(NoDataError)
        )

        val emittedStates = mutableListOf<SchoolListState>()

        // Use background scope as this scope is closed after the test is completed
        backgroundScope.launch { viewModel.stateFlow.toList(emittedStates) }

        viewModel.loadSchoolList()

        assertEquals(expectedStates, emittedStates)
    }
}