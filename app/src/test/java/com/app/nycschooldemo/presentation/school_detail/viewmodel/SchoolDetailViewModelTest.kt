package com.app.nycschooldemo.presentation.school_detail.viewmodel

import androidx.lifecycle.SavedStateHandle
import com.app.nycschooldemo.core.exceptions.NetworkUnavailable
import com.app.nycschooldemo.data.repository.SharedRepository
import com.app.nycschooldemo.domain.GetSchoolDetailUseCase
import com.app.nycschooldemo.presentation.school_detail.ui.dbnArgs
import com.app.nycschooldemo.presentation.school_list.viewmodel.SchoolInfo
import com.app.nycschooldemo.rule.ViewModelTestRule
import com.appmattus.kotlinfixture.kotlinFixture
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SchoolDetailViewModelTest {

    val dispatcher = UnconfinedTestDispatcher()

    @get:Rule
    val rule = ViewModelTestRule()
    val fixture = kotlinFixture()
    val dbn = fixture<String>()
    val schoolDetail = fixture<SchoolDetail>()
    val schoolInfo = fixture<SchoolInfo>()
    val initialState = SchoolDetailState()

    @MockK
    lateinit var getSchoolDetailUseCase: GetSchoolDetailUseCase
    @MockK
    lateinit var savedStateHandle: SavedStateHandle
    @MockK
    lateinit var sharedRepository: SharedRepository
    lateinit var viewModel: SchoolDetailViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        coEvery { getSchoolDetailUseCase(any()) } returns flowOf(schoolDetail)
        coEvery { sharedRepository.getCurrentSelectedValue() } returns schoolInfo
        every { savedStateHandle.get<String>(dbnArgs) } returns dbn
        viewModel = SchoolDetailViewModel(
            useCase = getSchoolDetailUseCase,
            state = initialState,
            dispatcher = dispatcher,
            stateHandle = savedStateHandle,
            sharedRepository = sharedRepository
        )
    }

    @Test
    fun `check emitted states on loading school detail successfully`() = runTest(dispatcher) {
        // The viewModel.loadSchoolDetail() action is called in constructor invocation and the states are emitted.
        // So we reset the initial state.
        viewModel.setState { initialState }

        // Represent the list of expected states including initial states
        val expectedStates = listOf(
            initialState,
            initialState.showLoading(),
            initialState.onSuccess(schoolDetail, schoolInfo)
        )

        // Capture all the emitted states
        val emittedStates = mutableListOf<SchoolDetailState>()

        // Use background scope as this scope is closed after the test is completed
        backgroundScope.launch {
            viewModel.stateFlow.toList(emittedStates)
        }
        viewModel.loadSchoolDetail(dbn)
        assertEquals(expectedStates, emittedStates)
    }

    @Test
    fun `check emitted states on error while loading school detail`() = runTest(dispatcher) {

        // throw error
        coEvery { getSchoolDetailUseCase(any()) } returns flow { throw NetworkUnavailable }
        // The viewModel.loadSchoolDetail() action is called in constructor invocation and the states are emitted.
        // So we reset the initial state.
        viewModel.setState { initialState }

        // Represent the list of expected states including initial states
        val expectedStates = listOf(
            initialState,
            initialState.showLoading(),
            initialState.onErrorResult(NetworkUnavailable)
        )

        // Capture all the emitted states
        val emittedStates = mutableListOf<SchoolDetailState>()

        // Use background scope as this scope is closed after the test is completed
        backgroundScope.launch {
            viewModel.stateFlow.toList(emittedStates)
        }
        viewModel.loadSchoolDetail(dbn)
        assertEquals(expectedStates, emittedStates)
    }
}