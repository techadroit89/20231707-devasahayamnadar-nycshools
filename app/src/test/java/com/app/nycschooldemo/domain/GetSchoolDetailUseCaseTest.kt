package com.app.nycschooldemo.domain

import com.app.nycschooldemo.core.exceptions.NoDataError
import com.app.nycschooldemo.data.datasource.api.response.SchoolDetailResponse
import com.app.nycschooldemo.data.repository.RemoteRepository
import com.app.nycschooldemo.presentation.school_detail.viewmodel.SchoolDetail
import com.appmattus.kotlinfixture.kotlinFixture
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetSchoolDetailUseCaseTest {

    @MockK
    private lateinit var remoteRepository: RemoteRepository
    private lateinit var usecase: GetSchoolDetailUseCase
    val fixture = kotlinFixture()
    val dbn = "02M260"
    val schoolDetailResponse = fixture<SchoolDetailResponse>()
    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        usecase = GetSchoolDetailUseCase(remoteRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `usecase should return SchoolDetail when data is available`() = runTest {

        val expectedSchoolDetail = SchoolDetail(
            schoolName = schoolDetailResponse.school_name,
            mathScore = schoolDetailResponse.sat_math_avg_score,
            writingScore = schoolDetailResponse.sat_writing_avg_score,
            readingScore = schoolDetailResponse.sat_critical_reading_avg_score
        )
        coEvery { remoteRepository.getSchoolDetail(dbn) } returns flowOf(listOf(schoolDetailResponse))

        // When
        val result: SchoolDetail = usecase(dbn).single()

        // Then
        assertEquals(expectedSchoolDetail, result)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test(expected = NoDataError::class)
    fun `usecase should throw NoDataError when data is not available`() = runTest {

        coEvery { remoteRepository.getSchoolDetail(dbn) } returns flowOf(emptyList())

        // When
        usecase(dbn).single()

        // Then - Exception should be thrown (NoDataError)
    }
}
