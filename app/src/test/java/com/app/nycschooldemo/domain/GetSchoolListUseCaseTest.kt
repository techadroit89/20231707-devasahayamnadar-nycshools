package com.app.nycschooldemo.domain

import com.app.nycschooldemo.core.exceptions.NetworkUnavailable
import com.app.nycschooldemo.data.datasource.api.response.SchoolListResponse
import com.app.nycschooldemo.data.repository.RemoteRepository
import com.app.nycschooldemo.presentation.school_list.viewmodel.SchoolInfo
import com.appmattus.kotlinfixture.kotlinFixture
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GetSchoolListUseCaseTest {

    private lateinit var remoteRepository: RemoteRepository
    private lateinit var getSchoolListUseCase: GetSchoolListUseCase
    val fixture = kotlinFixture()
    val schoolListResponse = fixture<List<SchoolListResponse>>()

    @Before
    fun setup() {
        remoteRepository = mockk()
        getSchoolListUseCase = GetSchoolListUseCase(remoteRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `UseCase should return a list of SchoolInfo when data is available`() = runTest {
        val expectedSchoolInfoList = schoolListResponse.map {
            SchoolInfo(
                name = it.school_name,
                description = it.overview_paragraph,
                dbn = it.dbn,
                phone = it.phone_number,
                latitude = it.latitude,
                longitude = it.longitude,
                address = "${it.primary_address_line_1} ${it.city} ${it.zip} ${it.state_code}",
                website = it.website
            )
        }
        coEvery { remoteRepository.getSchoolList() } returns flowOf(schoolListResponse)

        // When
        val result: List<SchoolInfo> = getSchoolListUseCase().single()

        // Then
        assertEquals(expectedSchoolInfoList, result)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test(expected = NetworkUnavailable::class)
    fun `UseCase should return an empty list when data is not available`() = runTest {
        coEvery { remoteRepository.getSchoolList() } returns throw NetworkUnavailable

        // When
        getSchoolListUseCase().single()

        // Then - Exception should be thrown (NoDataError)
    }
}
