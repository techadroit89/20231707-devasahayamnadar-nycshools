package com.app.nycschooldemo.data.repository

import com.app.nycschooldemo.data.datasource.api.response.SchoolDetailResponse
import com.app.nycschooldemo.data.datasource.api.response.SchoolListResponse
import com.app.nycschooldemo.data.datasource.api.service.NycApiService
import com.appmattus.kotlinfixture.kotlinFixture
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class RemoteRepositoryTest {

    @MockK
    private lateinit var apiService: NycApiService
    private lateinit var remoteRepository: RemoteRepository

    val fixture = kotlinFixture()
    val schoolListResponse = fixture<List<SchoolListResponse>>()
    val schoolDetailResponse = fixture<SchoolDetailResponse>()

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        remoteRepository = RemoteRepository(apiService)
    }

    @Test
    fun `getSchoolList should return a list of schools`() = runTest {

        coEvery { apiService.getSchoolList() } returns flowOf(schoolListResponse)

        val result: List<SchoolListResponse> = remoteRepository.getSchoolList().single()
        assertEquals(schoolListResponse, result)
    }

    @Test
    fun `getSchoolDetail should return school details`() = runTest {
        coEvery { apiService.getSchoolDetailsFor(any()) } returns flowOf(listOf(schoolDetailResponse))

        val result: List<SchoolDetailResponse> = remoteRepository.getSchoolDetail("123").single()
        assertEquals(listOf(schoolDetailResponse), result)
    }

}


