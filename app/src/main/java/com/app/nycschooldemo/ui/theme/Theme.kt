package com.app.nycschooldemo.ui.theme

import android.app.Activity
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat

private val DarkColorScheme = darkColorScheme(
    primary = primaryColorDark,
    primaryContainer = primaryVariantColorDark,
    onPrimary = onPrimary,
    secondary = secondaryColorDark,
    secondaryContainer = secondaryColorDark,
    onSecondaryContainer = accentColor,
    background = backgroundColorDark,
    surface = surfaceColorDark,
    surfaceVariant = surfaceColorDark,
    onBackground = onBackgroundColorDark,
    onSurface = onSurfaceColorDark,
    error = errorColor
)

private val LightColorScheme = lightColorScheme(
    primary = primaryColorLight,
    primaryContainer = primaryContainer,
    onPrimaryContainer = onPrimaryContainer,
    secondary = secondaryColorLight,
    secondaryContainer = surfaceColor,
    onSecondaryContainer = lightAccentColor,
    onPrimary = onPrimaryLight,
    background = surfaceColorLight,
    surface = primaryColorLight,
    surfaceVariant = surfaceVariant,
    onBackground = onBackgroundColorLight,
    onSurface = onSurfaceColorLight,
    error = errorColor
)

@Composable
fun NYCSchoolDemoTheme(
    // Due to Time constraint setting default theme as light theme
    darkTheme: Boolean = false,//isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {
    /// Add Side effects to change mobile status bar color
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = LightColorScheme.primary.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = darkTheme
        }
    }

    CompositionLocalProvider(
        LocalDimension provides getDefaultDimension()
    ) {
        MaterialTheme(
            colorScheme = LightColorScheme,
            typography = NYCTypography.create(darkTheme),
            content = content
        )
    }
}