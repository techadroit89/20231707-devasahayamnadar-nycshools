package com.app.nycschooldemo.ui.theme

import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

/**
 * Represent Dimension to be used in the app
 *
 *
 */
class Dimension(
    val paddingSmall: Dp,
    val paddingMedium: Dp,
    val paddingLarge: Dp,
    val spacingXSmall: Dp,
    val spacingSmall: Dp,
    val spacingMedium: Dp,
    val spacingLarge: Dp,
)

/**
 * factory method to create Dimension object
 */
fun dimension(
    paddingSmall: Dp,
    paddingMedium: Dp,
    paddingLarge: Dp,
    spacingXSmall: Dp,
    spacingSmall: Dp,
    spacingMedium: Dp,
    spacingLarge: Dp,
) = Dimension(
    paddingSmall,
    paddingMedium,
    paddingLarge,
    spacingXSmall,
    spacingSmall,
    spacingMedium,
    spacingLarge,
)

fun getDefaultDimension() = dimension(
    paddingSmall = 4.dp,
    paddingMedium = 8.dp,
    paddingLarge = 16.dp,
    spacingXSmall = 4.dp,
    spacingSmall = 8.dp,
    spacingMedium = 16.dp,
    spacingLarge = 24.dp
)


internal val LocalDimension = staticCompositionLocalOf { getDefaultDimension() }