package com.app.nycschooldemo.ui.theme

import androidx.compose.ui.graphics.Color

val accentColor = Color(0xFFfaa627)
val lightAccentColor = Color(0xFFf96163)
val errorColor = Color(0xffd00036)

val primaryColorDark = Color(0xFF4E342E)
val primaryVariantColorDark = Color(0xff3E2723)
val secondaryColorDark = Color(0xff3E2723)
val backgroundColorDark = Color(0xFF4E342E)
val surfaceColorDark = Color(0xFF5D4037)
val onBackgroundColorDark = Color(0xFFF5F5F5)
val onSurfaceColorDark = Color(0xFFF5F5F5)
val onPrimary = Color(0xFFF5F5F5)

val primaryColorLight = Color(0xFF006688)
val primaryVariantColorLight = Color(0xFF5f7481)
val primaryContainer = Color(0xFFc2e8ff)
val onPrimaryContainer = Color(0xFF001e2c)
val surfaceVariant = Color(0xFFdce3e9)
val primaryColor = Color(0xFF006688)
val secondaryColorLight = lightAccentColor
val backgroundColorLight = Color(0xFFfafafa)
val surfaceColorLight = Color.White
val onBackgroundColorLight = Color(0xFF000000)
val onSurfaceColorLight = Color(0xFF000000)
val onPrimaryLight = Color(0xFF000000)

val lightGrayColor = Color(0xFF9E9E9E)

// colors for use interest screen
val surfaceColor = Color(0xFFFEDBD0)
