package com.app.nycschooldemo.core

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.app.nycschooldemo.ui.theme.LocalDimension
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

inline fun <T> Flow<T>.collectIn(scope: CoroutineScope, crossinline action: suspend (T) -> Unit) {
    scope.launch {
        this@collectIn.collect {
            action(it)
        }
    }
}


@Composable
fun SmallSpacer(){
    Spacer(modifier = Modifier.height(LocalDimension.current.spacingSmall))
}
@Composable
fun LargeSpacer(){
    Spacer(modifier = Modifier.height(LocalDimension.current.spacingLarge))
}