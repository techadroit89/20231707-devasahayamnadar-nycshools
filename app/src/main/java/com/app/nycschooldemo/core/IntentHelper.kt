package com.app.nycschooldemo.core

import android.app.Activity
import android.content.Intent
import android.net.Uri

fun openGoogleMaps(schoolName:String,latitude: String, longitude: String, activity: Activity) {
    // Create a Uri from an intent string. Use the result to create an Intent.
    val gmmIntentUri = Uri.parse("geo:$latitude,$longitude?q=$schoolName")

    // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
    // Make the Intent explicit by setting the Google Maps package
    mapIntent.setPackage("com.google.android.apps.maps")
    // Attempt to start an activity that can handle the Intent
    activity.startActivity(mapIntent)
}

fun dialPhoneNumber(phoneNumber: String, activity: Activity) {
    val intent = Intent(Intent.ACTION_DIAL).apply {
        data = Uri.parse("tel:$phoneNumber")
    }
    if (intent.resolveActivity(activity.packageManager) != null) {
        activity.startActivity(intent)
    }
}