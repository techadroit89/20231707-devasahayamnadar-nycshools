package com.app.nycschooldemo.core

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

open class BaseViewModel<T>(initialState: T) : ViewModel() {
    private val _uiStateFlow = MutableStateFlow(initialState)

    val stateFlow = _uiStateFlow.asStateFlow()

    fun setState(reducer: T.() -> T){
        _uiStateFlow.update {
            reducer(it)
        }
    }

}