package com.app.nycschooldemo.di

import com.app.nycschooldemo.BuildConfig
import com.app.nycschooldemo.data.datasource.api.service.NycApiService
import com.app.nycschooldemo.data.datasource.api.service.adapter.FlowCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

const val TIMEOUT = 20L

/**
 * Hilt module to provide objects for network operations
 */
@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    // Provide an instance of OkHttpClient with a singleton scope
    @Singleton
    @Provides
    fun provideOkhttp() =
        OkHttpClient
            .Builder()
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .build()

    // Provide an instance of Retrofit with a singleton scope
    @Singleton
    @Provides
    fun provideRetrofit(okhttpClient: OkHttpClient): Retrofit = Retrofit
        .Builder()
        .baseUrl(BuildConfig.apiBaseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(FlowCallAdapterFactory())
        .client(okhttpClient)
        .build()

    // Provide an instance of NycApiService
    @Provides
    fun provideApiService(retrofit: Retrofit): NycApiService =
        retrofit.create(NycApiService::class.java)
}
