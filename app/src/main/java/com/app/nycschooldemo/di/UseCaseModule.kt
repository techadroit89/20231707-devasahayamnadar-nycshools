package com.app.nycschooldemo.di

import com.app.nycschooldemo.data.repository.RemoteRepository
import com.app.nycschooldemo.domain.GetSchoolDetailUseCase
import com.app.nycschooldemo.domain.GetSchoolListUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

/**
 * Hilt Module to provide UseCase instances
 */
@Module
@InstallIn(ViewModelComponent::class)
class UseCaseModule {

    @Provides
    fun provideSchoolDetail(repository: RemoteRepository) = GetSchoolDetailUseCase(repository)

    @Provides
    fun provideSchoolList(repository: RemoteRepository) = GetSchoolListUseCase(repository)
}