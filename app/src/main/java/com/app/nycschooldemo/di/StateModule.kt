package com.app.nycschooldemo.di

import com.app.nycschooldemo.presentation.main.viewmodel.MainState
import com.app.nycschooldemo.presentation.school_detail.viewmodel.SchoolDetailState
import com.app.nycschooldemo.presentation.school_list.viewmodel.SchoolListState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Hilt Module to provide instances that represents state of a ViewModel
 */
@Module
@InstallIn(SingletonComponent::class)
class StateModule {

    @Provides
    fun provideSchoolListState() = SchoolListState()

    @Provides
    fun provideSchoolDetailState() = SchoolDetailState()

    @Provides
    fun provideMainState() = MainState()
}