package com.app.nycschooldemo.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Qualifier

/**
 * Hilt module to provide Dispatchers
 */
@Module
@InstallIn(ViewModelComponent::class)
class DispatchersModule {

    @IODispatcher
    @Provides
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

}

/**
 * Named Qualifier for providing IODispatchers
 */
@Retention(AnnotationRetention.BINARY)
@Qualifier
annotation class IODispatcher