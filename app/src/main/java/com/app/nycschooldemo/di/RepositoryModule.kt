package com.app.nycschooldemo.di

import com.app.nycschooldemo.data.datasource.api.service.NycApiService
import com.app.nycschooldemo.data.repository.RemoteRepository
import com.app.nycschooldemo.data.repository.SharedRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Hilt Module to provide repositories instances
 */
@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    fun getRemoteRepository(apiService: NycApiService) = RemoteRepository(apiService)

    @Singleton
    @Provides
    fun getSharedRepository() = SharedRepository()
}