package com.app.nycschooldemo.data.repository

import com.app.nycschooldemo.data.datasource.api.service.NycApiService
import kotlinx.coroutines.flow.Flow

/**
 * A repository class that handles remote data operations for retrieving school information.
 *
 * @property apiService The API service used to communicate with the remote data source.
 */
class RemoteRepository(private val apiService: NycApiService) {

    /**
     * Retrieves a list of schools in Nyc.
     *
     * @return A [Flow] object representing the asynchronous network request for the school list.
     */
    fun getSchoolList() = apiService.getSchoolList()

    /**
     * Retrieves test score information about a specific school.
     *
     * @param dbn The unique identifier of the school.
     * @return A [Flow] object representing the asynchronous network request for the school details.
     */
    fun getSchoolDetail(dbn: String) = apiService.getSchoolDetailsFor(dbn)
}
