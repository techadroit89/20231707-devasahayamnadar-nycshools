package com.app.nycschooldemo.data.repository

import com.app.nycschooldemo.presentation.school_list.viewmodel.SchoolInfo
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update

/**
 * Repository to share data through out the application lifecycle.
 * This is mostly used to share data between composable screens
 * as passing a huge data through navigation is considered anti-pattern.
 * This data layer can be used to share the data between screens.
 * See [Navigation Doc](https://developer.android.com/jetpack/compose/navigation#retrieving-complex-data)
 */
class SharedRepository {
    private val _selectedSchoolInfoFlow = MutableStateFlow<SchoolInfo?>(null)
    val stateFlow = _selectedSchoolInfoFlow
    fun selectedSchoolInfo(schoolInfo: SchoolInfo) {
        _selectedSchoolInfoFlow.update { schoolInfo  }
    }

    fun clearSelection() {
        _selectedSchoolInfoFlow.update { null }
    }

    fun getCurrentSelectedValue() = _selectedSchoolInfoFlow.value
}