package com.app.nycschooldemo.data.datasource.api.service

import com.app.nycschooldemo.data.datasource.api.response.SchoolDetailResponse
import com.app.nycschooldemo.data.datasource.api.response.SchoolListResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Query

interface NycApiService {
    @GET("resource/s3k6-pzi2.json")
    fun getSchoolList(): Flow<List<SchoolListResponse>>

    @GET("resource/f9bf-2cp4.json")
    fun getSchoolDetailsFor(@Query("dbn") dbn: String): Flow<List<SchoolDetailResponse>>
}