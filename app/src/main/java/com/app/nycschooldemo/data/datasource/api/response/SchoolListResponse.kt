package com.app.nycschooldemo.data.datasource.api.response

/**
 * School List api response.
 */
data class SchoolListResponse(
    val dbn: String,
    val school_name: String,
    val overview_paragraph: String,
    val location: String,
    val phone_number: String,
    val fax_number: String,
    val school_email: String,
    val website: String,
    val total_students: String,
    val attendance_rate: String,
    val interest1: String,
    val method1: String,
    val primary_address_line_1:String,
    val city: String,
    val zip: String,
    val state_code: String,
    val latitude: String,
    val longitude: String,
    val community_board: String,
    val council_district: String,
    val census_tract: String,
    val bin: String,
    val bbl: String,
    val nta: String,
    val borough: String
)
