package com.app.nycschooldemo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.app.nycschooldemo.presentation.main.ui.MainScreen
import com.app.nycschooldemo.ui.theme.NYCSchoolDemoTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NYCSchoolDemoTheme {
                MainScreen()
            }
        }
    }
}
