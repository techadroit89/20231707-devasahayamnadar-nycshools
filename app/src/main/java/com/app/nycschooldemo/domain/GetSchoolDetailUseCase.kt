package com.app.nycschooldemo.domain

import com.app.nycschooldemo.core.exceptions.NoDataError
import com.app.nycschooldemo.data.repository.RemoteRepository
import com.app.nycschooldemo.presentation.school_detail.viewmodel.SchoolDetail
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

/**
 * Use case class for retrieving test score information about a school.
 *
 * @property remoteRepository The repository responsible for fetching remote school data.
 */
class GetSchoolDetailUseCase(private val remoteRepository: RemoteRepository) {

    /**
     * Invokes the use case to retrieve score information about a school.
     *
     * @param dbn The unique identifier of the school.
     * @return A [Flow] object representing the asynchronous network request for the school details.
     */
    operator fun invoke(dbn: String) = remoteRepository.getSchoolDetail(dbn).map {
        val response = it.firstOrNull() ?: throw NoDataError
        SchoolDetail(
            schoolName = response.school_name,
            mathScore = response.sat_math_avg_score,
            writingScore = response.sat_writing_avg_score,
            readingScore = response.sat_critical_reading_avg_score
        )
    }
}
