package com.app.nycschooldemo.domain

import com.app.nycschooldemo.data.repository.RemoteRepository
import com.app.nycschooldemo.presentation.school_list.viewmodel.SchoolInfo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

/**
 * Use case class for retrieving a list of schools in Nyc.
 *
 * @property remoteRepository The repository responsible for fetching remote school data.
 */
class GetSchoolListUseCase(private val remoteRepository: RemoteRepository) {

    /**
     * Invokes the use case to retrieve a list of schools in Nyc.
     *
     * @return A [Flow] object representing the asynchronous network request for the school list.
     */
    operator fun invoke() = remoteRepository.getSchoolList().map { list ->
        list.map {
            SchoolInfo(
                name = it.school_name,
                description = it.overview_paragraph,
                dbn = it.dbn,
                phone = it.phone_number,
                latitude = it.latitude,
                longitude = it.longitude,
                address = "${it.primary_address_line_1} ${it.city} ${it.zip} ${it.state_code}",
                website = it.website
            )
        }
    }
}
