package com.app.nycschooldemo.presentation.main.viewmodel

import androidx.lifecycle.viewModelScope
import com.app.nycschooldemo.core.BaseViewModel
import com.app.nycschooldemo.data.repository.SharedRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val sharedRepository: SharedRepository,
    initialState: MainState
) : BaseViewModel<MainState>(initialState) {

    init {
        viewModelScope.launch {
            sharedRepository.stateFlow.collect {
                setState { MainState(it?.name, it?.website) }
            }
        }
    }

    fun clearSelection(){
        sharedRepository.clearSelection()
    }
}