package com.app.nycschooldemo.presentation.main.ui

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.app.nycschooldemo.R
import com.app.nycschooldemo.presentation.main.viewmodel.MainViewModel
import com.app.nycschooldemo.presentation.school_detail.ui.navigateToSchoolDetail
import com.app.nycschooldemo.presentation.school_detail.ui.schoolDetailScreen
import com.app.nycschooldemo.presentation.school_list.ui.SCHOOL_LIST_ROUTE
import com.app.nycschooldemo.presentation.school_list.ui.schoolListScreen
import com.app.nycschooldemo.ui.theme.LocalDimension

const val SHOW_NAVIGATION = "show_navigation"

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(mainViewModel: MainViewModel = hiltViewModel()) {
    val navController = rememberNavController()
    Scaffold(
        topBar = { AppBar(navController, mainViewModel) }
    ) {
        MainContent(modifier = Modifier.padding(it), navController = navController)
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AppBar(navController: NavHostController, mainViewModel: MainViewModel) {
    val mainState by mainViewModel.stateFlow.collectAsStateWithLifecycle()
    TopAppBar(
        title = {
            AppBarTitle(schoolName = mainState.schoolName)
        },
        navigationIcon = {
            NavigationIcon(mainViewModel, navController)
        }
    )
}

@Composable
fun NavigationIcon(mainViewModel: MainViewModel, navController: NavHostController) {
    val navBackStackEntry: NavBackStackEntry? by navController.currentBackStackEntryAsState()
    val showNavigation = navBackStackEntry?.arguments?.getBoolean(SHOW_NAVIGATION) ?: false
    if (showNavigation)
        BackNavigationIcon {
            /// Need to handle the jank on text change when system back pressed
            mainViewModel.clearSelection()
            navController.navigateUp()
        }
}

@Composable
fun AppBarTitle(schoolName: String?) {
    Text(
        text = schoolName ?: stringResource(id = R.string.toolbar_title),
        style = MaterialTheme.typography.titleLarge,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        modifier = Modifier.padding(end = LocalDimension.current.paddingLarge)
    )
}

@Composable
fun MainContent(modifier: Modifier, navController: NavHostController) {
    Surface(
        modifier = modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background
    ) {
        NavHost(navController = navController, startDestination = SCHOOL_LIST_ROUTE) {
            schoolListScreen {
                navController.navigateToSchoolDetail(it)
            }
            schoolDetailScreen()
        }
    }
}

@Composable
fun BackNavigationIcon(onBackNavigation: () -> Unit) {
    Box(modifier = Modifier
        .clickable { onBackNavigation() }
        .padding(horizontal = LocalDimension.current.paddingSmall)) {
        Icon(
            Icons.Filled.ArrowBack,
            contentDescription = stringResource(id = R.string.back_nav_desc),
            tint = Color.White
        )
    }
}
