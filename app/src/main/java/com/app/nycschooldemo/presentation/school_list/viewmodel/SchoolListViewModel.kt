package com.app.nycschooldemo.presentation.school_list.viewmodel

import androidx.lifecycle.viewModelScope
import com.app.nycschooldemo.core.BaseViewModel
import com.app.nycschooldemo.core.collectIn
import com.app.nycschooldemo.data.repository.SharedRepository
import com.app.nycschooldemo.di.IODispatcher
import com.app.nycschooldemo.domain.GetSchoolListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    val useCase: GetSchoolListUseCase,
    val repository: SharedRepository,
    state: SchoolListState,
    @IODispatcher val dispatcher: CoroutineDispatcher
) : BaseViewModel<SchoolListState>(state) {

    init {
        loadSchoolList()
    }

    fun goToSchoolDetail(schoolInfo: SchoolInfo){
        repository.selectedSchoolInfo(schoolInfo)
    }

    fun loadSchoolList() {
        setState { showLoading() }
        useCase()
            .flowOn(dispatcher)
            .catch { setState { onErrorResult(it) } }
            .collectIn(viewModelScope) {
                setState {
                    onLoad(it)
                }
            }
    }
}