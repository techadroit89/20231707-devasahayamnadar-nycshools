package com.app.nycschooldemo.presentation.school_detail.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.app.nycschooldemo.core.BaseViewModel
import com.app.nycschooldemo.core.EMPTY_STRING
import com.app.nycschooldemo.core.collectIn
import com.app.nycschooldemo.data.repository.SharedRepository
import com.app.nycschooldemo.di.IODispatcher
import com.app.nycschooldemo.domain.GetSchoolDetailUseCase
import com.app.nycschooldemo.presentation.school_detail.ui.dbnArgs
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class SchoolDetailViewModel @Inject constructor(
    stateHandle: SavedStateHandle,
    state: SchoolDetailState,
    val useCase: GetSchoolDetailUseCase,
    val sharedRepository: SharedRepository,
    @IODispatcher val dispatcher: CoroutineDispatcher,
) : BaseViewModel<SchoolDetailState>(state) {
    // Get the school "dbn" argument from the SavedStateHandle or set it as EMPTY_STRING if not found
    private val dbn = stateHandle[dbnArgs] ?: EMPTY_STRING

    init {
        loadSchoolDetail(dbn)
    }

    /**
     * Retry the loading of the school detail with the same "dbn".
     */
    fun retry() {
        loadSchoolDetail(dbn)
    }

    /**
     * Load the school detail for the given "dbn".
     *
     * @param dbn The unique identifier of the school.
     */
    fun loadSchoolDetail(dbn: String) {
        setState { showLoading() }
        // Invoke the use case to get the school detail using the provided "dbn"
        useCase(dbn)
            .map {
                val schoolInfo = sharedRepository.getCurrentSelectedValue()
                Pair(it, schoolInfo)
            }
            .flowOn(dispatcher) // Perform the operation on the IO dispatcher
            .catch {
                // Handle any errors during the flow collection and set the error state
                setState { onErrorResult(it) }
            }
            .collectIn(viewModelScope) {
                // Collect the flow and set the success state with the received school detail
                setState { onSuccess(it.first, it.second) }
            }
    }

    override fun onCleared() {
        super.onCleared()
        sharedRepository.clearSelection()
    }
}
