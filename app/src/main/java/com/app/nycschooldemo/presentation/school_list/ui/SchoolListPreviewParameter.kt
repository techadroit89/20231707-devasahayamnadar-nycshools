package com.app.nycschooldemo.presentation.school_list.ui

import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.app.nycschooldemo.presentation.school_list.viewmodel.SchoolInfo

class SchoolListPreviewParameter : PreviewParameterProvider<SchoolInfo> {

    override val values: Sequence<SchoolInfo> = sequence {
        SchoolInfo(
            "Epic High School", "", "",
            "", "", "", "", ""
        )
        SchoolInfo(
            "Clinton School Writers & Artists, M.S. 260", "",
            "", "", "", "", "", ""
        )
        SchoolInfo(
            "Preparatory Academy for Writers: A College Board School",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        )
        SchoolInfo(
            "Benjamin Franklin High School For Finance & Information Technology",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        )
    }
}