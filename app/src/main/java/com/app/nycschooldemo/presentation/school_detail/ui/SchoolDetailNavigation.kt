package com.app.nycschooldemo.presentation.school_detail.ui

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.app.nycschooldemo.presentation.main.ui.SHOW_NAVIGATION

const val dbnArgs = "dbn"
const val SCHOOL_DETAIL_PATH = "school_detail"
const val SCHOOL_DETAIL_ROUTE = "$SCHOOL_DETAIL_PATH/{$dbnArgs}/{$SHOW_NAVIGATION}"
fun NavHostController.navigateToSchoolDetail(dbn: String, showNavigationUp: Boolean = true) {
    this.navigate("$SCHOOL_DETAIL_PATH/${dbn}/${showNavigationUp}")
}

fun NavGraphBuilder.schoolDetailScreen() {
    composable(SCHOOL_DETAIL_ROUTE, arguments = listOf(
        navArgument(dbnArgs) { type = NavType.StringType },
        navArgument(SHOW_NAVIGATION) { type = NavType.BoolType },
    )) {
        SchoolDetailScreen()
    }
}