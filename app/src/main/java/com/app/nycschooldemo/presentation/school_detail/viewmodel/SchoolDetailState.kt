package com.app.nycschooldemo.presentation.school_detail.viewmodel

import com.app.nycschooldemo.core.EMPTY_STRING
import com.app.nycschooldemo.core.exceptions.ErrorResult
import com.app.nycschooldemo.core.exceptions.UnknownErrorResult
import com.app.nycschooldemo.presentation.school_list.viewmodel.SchoolInfo

data class SchoolDetailState(
    val schoolDetail: SchoolDetail = SchoolDetail(),
    val schoolInfo: SchoolInfo? = null,
    val isLoading: Boolean = false,
    val errorResult: ErrorResult? = null
)

/**
 *  Show loading state while fetching the school detail
 */
fun SchoolDetailState.showLoading() = copy(isLoading = true, errorResult = null)

fun SchoolDetailState.onErrorResult(errorResult: Throwable?): SchoolDetailState {

    if (errorResult !is ErrorResult)
        return copy(errorResult = UnknownErrorResult, isLoading = false)

    return copy(errorResult = errorResult, isLoading = false)
}

fun SchoolDetailState.onSuccess(schoolDetail: SchoolDetail, schoolInfo: SchoolInfo?) =
    copy(
        schoolDetail = schoolDetail,
        isLoading = false,
        errorResult = null,
        schoolInfo = schoolInfo
    )

data class SchoolDetail(
    val schoolName: String = EMPTY_STRING,
    val mathScore: String = EMPTY_STRING,
    val writingScore: String = EMPTY_STRING,
    val readingScore: String = EMPTY_STRING
)