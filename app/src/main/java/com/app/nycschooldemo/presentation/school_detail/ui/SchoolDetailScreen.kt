package com.app.nycschooldemo.presentation.school_detail.ui

import android.app.Activity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Phone
import androidx.compose.material.icons.outlined.Place
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.app.nycschooldemo.R
import com.app.nycschooldemo.core.LargeSpacer
import com.app.nycschooldemo.core.SmallSpacer
import com.app.nycschooldemo.core.dialPhoneNumber
import com.app.nycschooldemo.core.openGoogleMaps
import com.app.nycschooldemo.presentation.common_ui.ErrorScreen
import com.app.nycschooldemo.presentation.common_ui.LoadingView
import com.app.nycschooldemo.presentation.school_detail.viewmodel.SchoolDetail
import com.app.nycschooldemo.presentation.school_detail.viewmodel.SchoolDetailViewModel
import com.app.nycschooldemo.presentation.school_list.viewmodel.SchoolInfo
import com.app.nycschooldemo.ui.theme.LocalDimension

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SchoolDetailScreen(viewModel: SchoolDetailViewModel = hiltViewModel()) {
    val state by viewModel.stateFlow.collectAsStateWithLifecycle()

    Scaffold(
        floatingActionButton = { state.schoolInfo?.let { FabButton(state.schoolInfo) } }
    ) {
        Box(modifier = Modifier.padding(it)) {
            if (state.isLoading) {
                LoadingView()
            } else {
                Box {
                    SchoolDetailBody(
                        schoolInfo = state.schoolInfo,
                        schoolDetail = state.schoolDetail
                    )
                }
            }
            ErrorScreen(errorResult = state.errorResult) {
                viewModel.retry()
            }
        }
    }
}

@Composable
fun FabButton(schoolInfo: SchoolInfo?) {
    val activity = LocalContext.current as Activity
    Column {
        schoolInfo?.phone?.let { CallFabButton(phone = it, activity = activity) }
        Spacer(modifier = Modifier.height(LocalDimension.current.spacingSmall))
        if (schoolInfo?.latitude != null && schoolInfo.longitude != null) {
            MapFabButton(
                name = schoolInfo.name,
                latitude = schoolInfo.latitude,
                longitude = schoolInfo.longitude,
                activity = activity
            )
        }
    }
}

@Composable
fun CallFabButton(phone: String, activity: Activity) {
    FloatingActionButton(onClick = {
        /// Handle Empty Values
        dialPhoneNumber(phone, activity)
    }) {
        Icon(
            Icons.Outlined.Phone,
            contentDescription = stringResource(id = R.string.call_fab_desc),
            tint = MaterialTheme.colorScheme.onPrimaryContainer
        )
    }
}

@Composable
fun MapFabButton(name: String, latitude: String, longitude: String, activity: Activity) {
    FloatingActionButton(onClick = {
        /// Handle Empty Values
        openGoogleMaps(
            name,
            latitude,
            longitude,
            activity
        )
    }) {
        Icon(
            Icons.Outlined.Place,
            contentDescription = stringResource(id = R.string.map_fab_desc),
            tint = MaterialTheme.colorScheme.onPrimaryContainer
        )
    }
}

@Composable
fun SchoolDetailBody(schoolInfo: SchoolInfo?, schoolDetail: SchoolDetail) {

    if (schoolDetail.schoolName.isEmpty())
        return

    LazyColumn(modifier = Modifier.padding(LocalDimension.current.paddingLarge)) {
        schoolDetail(schoolInfo = schoolInfo, schoolDetail = schoolDetail)
    }
}

fun LazyListScope.schoolDetail(schoolInfo: SchoolInfo?, schoolDetail: SchoolDetail) {
    item {
        schoolInfo?.let {
            SchoolOverView(overView = it.description, address = it.address)
        }
        Spacer(modifier = Modifier.height(LocalDimension.current.paddingMedium))
        ScoresGrid(schoolDetail = schoolDetail)
    }
}

@Composable
fun SchoolOverView(overView: String, address: String) {
    Text(text = overView, style = MaterialTheme.typography.bodyMedium)
    SmallSpacer()
    AddressView(address = address)
    LargeSpacer()
}

@Composable
fun AddressView(address: String) {
    Row {
        Icon(
            Icons.Outlined.Place,
            contentDescription = stringResource(id = R.string.map_fab_desc),
            tint = MaterialTheme.colorScheme.onPrimary
        )
        SmallSpacer()
        Text(text = address, style = MaterialTheme.typography.bodyMedium)
    }
}

@Composable
fun ScoresGrid(modifier: Modifier = Modifier, schoolDetail: SchoolDetail) {
    Column {
        Text(
            text = stringResource(id = R.string.sat_scores),
            style = MaterialTheme.typography.displayLarge,
        )
        SmallSpacer()
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp))
                .background(MaterialTheme.colorScheme.surfaceVariant)
                .padding(
                    horizontal = LocalDimension.current.paddingMedium,
                    vertical = LocalDimension.current.paddingMedium,
                )
                .wrapContentHeight(),
            contentAlignment = Alignment.Center,
        ) {
            LazyRow(
                modifier = modifier.padding(vertical = LocalDimension.current.paddingLarge),
                horizontalArrangement = Arrangement.spacedBy(LocalDimension.current.paddingLarge),
            ) {
                scoreItem(title = R.string.math_score, score = schoolDetail.mathScore)
                scoreItem(title = R.string.reading_score, score = schoolDetail.readingScore)
                scoreItem(title = R.string.writing_score, score = schoolDetail.writingScore)
            }
        }
    }
}

fun LazyListScope.scoreItem(title: Int, score: String) {
    item {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(text = stringResource(id = title), style = MaterialTheme.typography.titleMedium)
            SmallSpacer()
            Text(text = score, style = MaterialTheme.typography.labelLarge)
        }
    }
}

