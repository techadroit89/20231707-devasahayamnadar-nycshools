package com.app.nycschooldemo.presentation.school_list.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.app.nycschooldemo.presentation.common_ui.ErrorScreen
import com.app.nycschooldemo.presentation.common_ui.LoadingView
import com.app.nycschooldemo.presentation.common_ui.SchoolNameView
import com.app.nycschooldemo.presentation.school_list.viewmodel.SchoolInfo
import com.app.nycschooldemo.presentation.school_list.viewmodel.SchoolListViewModel
import com.app.nycschooldemo.ui.theme.LocalDimension

@Composable
fun SchoolListScreen(
    viewModel: SchoolListViewModel = hiltViewModel<SchoolListViewModel>(),
    onSchoolListItemClicked: (String) -> Unit
) {
    val state by viewModel.stateFlow.collectAsStateWithLifecycle()
    if (state.isLoading) {
        LoadingView()
    } else {
        Surface(
            color = MaterialTheme.colorScheme.background
        ) {
            SchoolListContent(schoolList = state.list){
                viewModel.goToSchoolDetail(it)
                onSchoolListItemClicked(it.dbn)
            }
        }
    }
    ErrorScreen(errorResult = state.errorResult) {
        viewModel.loadSchoolList()
    }
}

@Composable
fun SchoolListContent(schoolList: List<SchoolInfo>, onSchoolListItemClicked: (SchoolInfo) -> Unit) {
    LazyColumn {
        items(schoolList.size) {
            val item = schoolList[it]
            SchoolListItem(schoolInfo = item, onSchoolListItemClicked = onSchoolListItemClicked)
        }
    }
}

@Composable
fun SchoolListItem(schoolInfo: SchoolInfo, onSchoolListItemClicked: (SchoolInfo) -> Unit) {
    Column(modifier = Modifier
        .padding(
            vertical = LocalDimension.current.paddingSmall,
            horizontal = LocalDimension.current.paddingLarge
        )
        .clickable { onSchoolListItemClicked(schoolInfo) }) {
        SchoolNameView(name = schoolInfo.name)
        Spacer(modifier = Modifier.height(LocalDimension.current.spacingXSmall))
        SchoolOverView(description = schoolInfo.description)
        Spacer(modifier = Modifier.height(LocalDimension.current.spacingSmall))
        Divider()
    }
}

@Composable
fun SchoolOverView(description: String) {
    Text(
        text = description,
        style = MaterialTheme.typography.bodyMedium,
        maxLines = 3,
        overflow = TextOverflow.Ellipsis
    )
}

@Preview
@Composable
fun SchoolListContentPreview(
    @PreviewParameter(SchoolListPreviewParameter::class) schoolInfo: SchoolInfo
) {
    SchoolListItem(schoolInfo = schoolInfo) {}
}