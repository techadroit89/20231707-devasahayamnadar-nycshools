package com.app.nycschooldemo.presentation.main.viewmodel

data class MainState(val schoolName: String? = null, val schoolWebsite: String? = null)