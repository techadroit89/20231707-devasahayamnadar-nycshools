package com.app.nycschooldemo.presentation.school_list.viewmodel

import com.app.nycschooldemo.core.exceptions.ErrorResult
import com.app.nycschooldemo.core.exceptions.UnknownErrorResult

data class SchoolListState(
    val list: List<SchoolInfo> = emptyList(),
    val isLoading: Boolean = false,
    val errorResult: ErrorResult? = null
)

fun SchoolListState.showLoading() = copy(isLoading = true, errorResult = null)

fun SchoolListState.onLoad(schoolList: List<SchoolInfo>) =
    copy(list = schoolList, isLoading = false)

fun SchoolListState.onErrorResult(errorResult: Throwable?): SchoolListState {

    if (errorResult !is ErrorResult)
        return copy(errorResult = UnknownErrorResult, isLoading = false)

    return copy(errorResult = errorResult, isLoading = false)
}

data class SchoolInfo(
    val name: String,
    val description: String,
    val dbn: String,
    val latitude: String?,
    val longitude: String?,
    val phone: String,
    val address:String,
    val website:String,
)