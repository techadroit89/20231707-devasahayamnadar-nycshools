package com.app.nycschooldemo.presentation.school_list.ui

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable

const val SCHOOL_LIST_ROUTE = "school_list"

fun NavGraphBuilder.schoolListScreen(
    onSchoolListItemClicked: (String) -> Unit
){
    composable(SCHOOL_LIST_ROUTE){
        SchoolListScreen(onSchoolListItemClicked = onSchoolListItemClicked)
    }
}