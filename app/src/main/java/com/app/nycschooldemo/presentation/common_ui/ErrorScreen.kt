package com.app.nycschooldemo.presentation.common_ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.app.nycschooldemo.R
import com.app.nycschooldemo.core.exceptions.ClientRequestErrorResult
import com.app.nycschooldemo.core.exceptions.ErrorResult
import com.app.nycschooldemo.core.exceptions.NetworkUnavailable
import com.app.nycschooldemo.core.exceptions.NoDataError
import com.app.nycschooldemo.core.exceptions.ServerResponseErrorResult
import com.app.nycschooldemo.ui.theme.LocalDimension

typealias OnRetry = () -> Unit

/**
 * Represents the Error Screen. The text and the button are shown according to the error result.
 *
 * @param errorResult The error result received. Can be null if there's no error.
 * @param retry Callback to be invoked when the user clicks the retry button.
 */
@Composable
fun ErrorScreen(errorResult: ErrorResult?, retry: OnRetry) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(
                horizontal = LocalDimension.current.paddingLarge,
                vertical = LocalDimension.current.paddingLarge
            ),
        contentAlignment = Alignment.Center
    ) {
        // Check if there's an error result and display the appropriate error message and retry button.
        errorResult?.let {
            val (text, showRetryOption) = when (it) {
                is NoDataError -> stringResource(id = R.string.no_data_available) to false
                is NetworkUnavailable -> stringResource(id = R.string.no_network_available) to true
                is ClientRequestErrorResult -> stringResource(id = R.string.request_param_error) to true
                is ServerResponseErrorResult -> stringResource(id = R.string.server_error) to true
                else -> stringResource(id = R.string.unknown_error) to true
            }
            ErrorWithRetry(text = text, showRetryOption) {
                retry() // Call the provided callback function to retry the operation.
            }
        }
    }
}

/**
 * Represents the Error with Retry options.
 *
 * @param text The error message text to be displayed.
 * @param showRetryOptions Boolean flag to indicate whether to show the retry button.
 * @param retry Callback to be invoked when the user clicks the retry button.
 */
@Composable
fun ErrorWithRetry(text: String, showRetryOptions: Boolean, retry: OnRetry) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text, style = MaterialTheme.typography.displayLarge)
        Spacer(modifier = Modifier.height(LocalDimension.current.spacingSmall))
        if (showRetryOptions)
            Button(onClick = { retry() }) {
                Text(text = "Retry", style = MaterialTheme.typography.labelMedium)
            }
    }
}
