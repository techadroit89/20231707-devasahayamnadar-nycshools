package com.app.nycschooldemo.presentation.common_ui

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable

@Composable
fun SchoolNameView(name:String){
    Text(text = name, style = MaterialTheme.typography.displayLarge)
}