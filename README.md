<h1>Nyc App</h1>

<p>A demo Nyc App which shows list of school in NYC. The app is build using Jetpack Compose and Hilt based on MVI and clean architecture. </p>
<br></br>

## Tech stack & Open-source libraries </br>

- Minimum SDK level 24 </br>
- [Kotlin](https://kotlinlang.org/)
  based 
- [Coroutines](https://github.com/Kotlin/kotlinx.coroutines) 
- [Flow](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/)
  for asynchronous.
- [Hilt](https://developer.android.com/training/dependency-injection/hilt-android) for dependency injection.</br>
- JetPack</br>
  -- [Compose](https://developer.android.com/jetpack/compose?gclid=CjwKCAjw64eJBhAGEiwABr9o2BRlFiZCYMbzXNFymS-fc9pxa66UPYGxS_MqRZsfAQnok2Dxyw-RgRoCHCsQAvD_BwE&gclsrc=aw.ds) -
  A modern toolkit for building native Android UI.</br>
  -- ViewModel - UI related data holder, lifecycle aware.</br>
- Architecture</br>
  -- MVI Architecture (Model - view - intent)</br>
  -- Clean Architecture (Repository and Usecase pattern)</br>
  -- StateFlow to Observe State</br>
- Material Design 3</br>
- [Retrofit2 & OkHttp3](https://github.com/square/retrofit) - construct the REST APIs.

<table>
    
  <td>
    <p>
<h1>School Detail Screen</h1>
      [<img src="../screenshot/screen_detail.png" alt="Recipe Home Screen" width="400" height="800"/>]
    </p>
  </td>
  <td>
    <p>
<h1>SchoolDetail Screen</h1>
      <img src="../screenshot/screen_list.png" alt="Recipe Video Screen" width="400" height="800"/>
    </p>
  </td>

</table>



